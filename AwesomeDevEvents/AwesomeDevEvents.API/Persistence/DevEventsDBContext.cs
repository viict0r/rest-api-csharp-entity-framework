﻿using AwesomeDevEvents.API.Entities;

namespace AwesomeDevEvents.API.Persistence
{
    public class DevEventsDBContext
    {
        public List<DevEvent> DevEvents { get; set; }

        public DevEventsDBContext() 
        {
            DevEvents = new List<DevEvent>();
        }
    }
}
